namespace NSU_Stack
{
	template<typename T>
	T node<T>::get_data() const
	{
		return this->data;
	}

	template<typename T>
	void head_insert(const T& entry, node<T>*& head)
	{
		node<T>* newNode;
		
		newNode = new node<T>(entry, head);					

		head = newNode;
	}

	template<typename T>
	void clear(node<T>*& head)
	{				
		while(head != NULL)
		{				
			node<T>* temp = head;								
			head = head->link();
			delete temp;								
		}		
	}
}