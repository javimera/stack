/*
	Programmer: Javier Mera
	Date modified: 11/2/2013
	File: sequence.h to declare sequence with the appropriate methods and variables to manipulate it as a stack. 
	      This sequence is made a template class in order to handle different types of stacks (int, float, etc...)		

	// CONSTRUCTORS
		
		Default Constructor
		sequence()
			initializes head_ptr to NULL
			initializes how_many to 0

		Copy Constructor
		sequence(const sequence<T>& )
			Precondition: A valid sequence to be used as initializer for the calling sequence object.
			Postcondition: The calling sequence object contains the same information as the sequence object parameter

	// DESTRUCTORS

		~sequence()
			Returns all the dynamic allocated memory back to the heap.
			This is called when the sequence object goes out of scope.

	// PUBLIC CLASS-MEMBER METHODS

		bool is_empty() const
			Returns true if the sequence's head_ptr points to node in the sequence.
			Returns false if the sequence's head_ptr points to NULL, meaning there are no nodes in the sequence.
			The const keyword does not allow the method to modify the calling sequence object.

		T top () const
			Method to return the head's data field
			Precondition: The calling sequence object must have at least one node in the sequence to return its' data field of type T
			The const keyword does not allow the method to modify the calling sequence object.

		void push(const T&)
			Postcondition: Inserts a new node in the sequence, and now the head_ptr points to the new node.

		T pop()
			Method to remove an item from the sequence.
			Precondition: At least one node has to be in the sequence.
			Postcondition: The node pointed by head_ptr has been removed from the sequence.
			               The head_ptr now points to its' link field.

			Function returns the deleted node's data field value;

		void print_sequence() const
			Method to print an entire sequence.
			Precondition: The calling sequence must have at least one node.
			The const keyword does not allow the method to modify the calling sequence object.

	// PRIVATE CLASS-MEMBER VARIABLES
		
		node<T>* head_ptr
			Member of type node<T> to hold a reference to the sequence's head node.

		size_t how_many
			Member of type size_t to hold the number of nodes in the sequence

	// PRIVATE CLASS-MEMBER METHOD(s)

		void copy_data_fields(T*&, node<T>*)
			Precondition: When copying/assignning, the source sequence must be non-empty.
			Postcondition: A new dynamic array has been filled with each of the source sequence's node's data field.
*/


#ifndef sequence_h_
#define sequence_h_

#include "node.h"
#include <string>
#include <iostream>
#include <sstream>

namespace NSUOK_Data_Structures
{
	template <typename T>
	class sequence
	{
	public:
		// CONSTRUCTORs
		sequence();
		sequence(const sequence<T>&);
		sequence<T> operator=(const sequence<T>&);

		// DESTRUCTOR
		~sequence();

		// CLASS MEMBER METHODS
		bool is_empty() const;		
		T top() const;	
		void push(const T&); 		
		T pop();
		void print_sequence() const
		{
			print(head_ptr);
		}		
		T evaluate_expression(std::string);
		std::string InfixToPostfix(std::string);

	private:		
		node<T>* head_ptr;
		size_t how_many;				
		void copy_data_fields(T*&, node<T>*);	
		void perform_operation(const char, T&, T) const;
		
	};
}

#include "sequence.cpp"
#endif