namespace NSUOK_Data_Structures
{
	template <typename T>
	// Sequence constructor
	sequence<T>::sequence()
	{
		// Set the head pointer to point to NULL for initialization
		this->head_ptr = NULL;

		// Set how many nodes to zero for initialization
		this->how_many = 0;
	}

	template <typename T>
	// Sequence copy constructor
	sequence<T>::sequence(const sequence<T>& source)
	{ 
		// Set the current sequence's head to NULL
		this->head_ptr = NULL;

		// Set the current sequence's how many to the source's how many nodes
		this->how_many = source.how_many;

		if(!source.is_empty())
		{
			// Create a temporary dynamic array to hold each source's node's data field in the order they were added in the first place
			T* numbers = new T[this->how_many];						

			// Call helper function to fill the array 
			this->copy_data_fields(numbers, source.head_ptr);

			// Insert in reverse order each array's number into the current sequence object
			for(int i = this->how_many - 1; i >= 0 ; i--)
			{
				head_insert(numbers[i], this->head_ptr);
			}

			// Delete the dynamically allocated array
			delete numbers;
		}
	}

	template <typename T>
	void sequence<T>::copy_data_fields(T*& numbers, node<T>* temp)
	{
		// Create an index variable to store a value in each array's position
		int index = 0;

		// Loop while there are nodes pointed by temp
		while(temp != NULL)
		{
			// Store the top number pointed by temp into the array
			numbers[index] = temp->get_data();

			// Go to the next node
			temp = temp->link();

			// Increment the index
			index++;
		}		
	}

	template <typename T>	
	sequence<T> sequence<T>::operator=(const sequence<T>& source)
	{
		// Call the copy constructor of the sequence being created
		this(source);		
	}

	template <typename T>	
	sequence<T>::~sequence()
	{
		// Call the clear method from our node class to delete all nodes
		clear(this->head_ptr);

		// Set how many nodes to zero when clearing out a sequence
		this->how_many = 0;
	}

	template <typename T>
	// Sequence method to check if the sequence is empty
	bool sequence<T>::is_empty() const
	{
		// Returns true if the sequence contains at least 1 node
		return this->head_ptr == NULL;
	}

	template <typename T>	
	T sequence<T>::top() const
	{
		// Check if there is data to return from the sequence
		if(!this->is_empty())
		{
			return this->head_ptr->get_data();
		}
	}

	template <typename T>
	// Sequence push method to insert a node
	void sequence<T>::push(const T& entry)
	{
		head_insert(entry, this->head_ptr);

		// Increment how many since we added a new node
		this->how_many++;
	}

	template <typename T>	
	T  sequence<T>::pop()
	{
		// Check if there is anything to pop from the sequence
		if(!this->is_empty())
		{
			// Create a temp node object to hold the current head
			node<T>* temp = this->head_ptr;

			// Assign the head its' link field
			this->head_ptr = this->head_ptr->link();

			// store the data pointed by temp
			int data = temp->get_data();

			// delete the temp node
			delete temp;

			// Decrement the amount of nodes in the sequence after deletion
			this->how_many--;

			// return the data
			return data;
		}
	}	

	template <typename T>
	std::string sequence<T>::InfixToPostfix(std::string expression)
	{
		// Define a second stack to hold the symbol operators
		sequence<char> operators;

		// Define a string variable to where the postfix notation will be saved
		std::string postfix = "";
		
		// Loop through the entire infix expression
		for(int i = 0 ; i < expression.length() ; i++)
		{											
			// Check if the next character is a symbol operator
			if(strchr("+-/*", expression[i]))
			{					
				// If its a symbol operator then print the top symbol operator from the stack, then pop it out.
				// Only enter the loop if the top operator in the stack is not a left parenthesis
				while( operators.top() != '(' )
				{					
					// If the top operator is not a left parenthesis, still check if the stack is empty and the order of precedence.
					// If it's empty, then break out of the loop.
					// If the top operator in the stack is of higher precedance than the next character in the expression, then break out of the loop.
					if(operators.is_empty() || (strchr("+-",operators.top()) && (strchr("*/", expression[i])) ))
						break;

					// In each loop, pop the top item and print it to the postfix string
					postfix += operators.pop();
					
					// After printing each symbol, print a space to follow the postfix notation
					postfix += " ";						
				}

				// Push the next character in the expression into the operator stack.
				operators.push(expression[i]);
			}	
			// Check if the next character is a numeric or letter variable
			else if(isalnum(expression[i]))
			{				
				// loop until the next character is not numeric or a letter variable
				// The loop will allow to store numbers of 2 or more digits
				while(isalnum(expression[i]))
				{			
					postfix += expression[i];
					i++;
				}				

				// After hitting a non numeric value in the expression, decrement the counter to evaluate this character
				i--;

				// After printing a digit or variable, print a space to follow the postfix notation
				postfix += " ";
			}	
			// Check if the next character is a left parenthesis
			else if(expression[i] == '(')
			{				
				// If it's a left parenthesis, then push into the operators stack
				// This will allow to read an expression accordingly if digits/operators are in between parenthesis
				operators.push(expression[i]);
			}
			// Check if the next character is a right parenthesis			
			else if( expression[i] == ')')
			{							
				// If it's a right parenthesis, then print and pop everything from the stack until we find its' correspondant left parenthesis
				while(operators.top() != '(' )
				{
					// In each loop, pop the top item and print it to the postfix string
					postfix += operators.pop();

					// After printing each symbol, print a space to follow the postfix notation
					postfix += " ";								
				}

				// When reaching the left parenthesis from the previous loop, pop it as we are done with that operand in between parenthesis
				operators.pop();
			}
			
		}

		// After reading the whole expression, check if there are operators in the stack
		while(!operators.is_empty())
		{
			postfix += operators.pop();			
		}

		// Return the expression in postfix notation
		return postfix;
	}

	template <typename T>
	T sequence<T>::evaluate_expression(std::string expression)
	{
		// Store the postfix expression in a string variable
		string postfix_expression = this->InfixToPostfix(expression);		

		// Declare a variable of type T to hold the second popped item from the stack
		// The second item popped from the stack will be our first operand in our calculations
		T first_operand;

		// Declare a variable of type T to hold the first popped item from the stack
		// The first item popped from the stack will be our second operand in our calculations
		T second_operand;

		// Loop through the entire postfix expression
		for(int i = 0 ; i < postfix_expression.length() ; i++)
		{
			// Declare a string variable in each loop to hold the next number in the expression
			// A string variable is needed since we may encounter a number with 2 or more digits.
			std::string variable ="";

			// Declare a variable of type T to hold the conversion of the number from a string to a T type.
			T number;

			// If we encounter a digit in the postfix expression, then loop until we find a nonnumeric character.
			while(isdigit(postfix_expression[i]))
			{
				// Store each numeric character into the string variable
				variable += postfix_expression[i];

				// Increment the postfix index
				i++;

				// Check if the whole number has been stored
				if(!isdigit(postfix_expression[i]))
				{
					// Create an istringstream to put the string into a stream to be able to convert it.
					istringstream stream (variable);			

					// Store the string number into number variable of type T
					stream >> number;

					// push the converted number into the stack.
					this->push(number);
				}
			}

			// If we dont encounter a digit, check if a symbol operator has been reached.
			if(strchr("/*-+",postfix_expression[i]))
			{
				// Store temporarily the symbol operator into a character variable
				char operation = postfix_expression[i];			

				// Pop the top number from the stack and store it as a second operand
				second_operand = this->pop();

				// Pop the next top number from the stack and store it as a first operand
				first_operand = this->pop();				

				// Call method to perform a switch statement and evaluate the operator
				this->perform_operation(operation, first_operand, second_operand);

				// push the first operand as it is the result of the operation previously performed.
				this->push(first_operand);
			}
		}
		
		// Return the top number from the stack which shuold be the final result.
		return this->top();
	}

	template <typename T>
	void sequence<T>::perform_operation(const char operation, T& operan1, T operand2) const
	{
		// use a switch statement to see which operation should be performed
		switch(operation)
		{
		case '+':	
			// Use the first operand as an accumulator and add the second operand to it.
			operan1 += operand2;					
			break;
		case '-':
			// Use the first operand as an accumulator and subctract the second operand from it.
			operan1 -= operand2;
			break;
		case '/':					
			// Use the first operand as an accumulator and divide the second over it.
			operan1 /= operand2;
			break;
		case '*':
			// Use the first operand as an accumulator and multiply the second operand to it.
			operan1 *= operand2;					
			break;
		}
	}
}