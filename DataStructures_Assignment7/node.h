/*
	Programmer: Javier Mera
	Date Modified: 11/10/2013
	File: 
		node.h header file to declare a node class in order to work with a stack using node objects.

	// CONSTRUCTOR
		
		node(const T&, node<T>*)
			Postcondition: A node has been initialized with a value of type T, and its' link pointing to a node of type node<T>

	// CLASS-MEMBER FUNCTIONS

		const node<T>* link() const
			Member function that returns a const link to the calling node. 
			This function is activated by const node, and will not allow the calling node to change its' data.

		node<T>* link()
			Member function that returns a non const link to the calling node.
			This function is activated by a non const node, and will allow the calling node to change its' data.

		T get_data() const;
			Member function that returns the private member data field.
			The calling node will not be able to change its' data because of the const keyword at the end of the function prototype.

	// NON-CLASS MEMBER FUNCTIONS
			
			template<typename T>
			void head_insert(const T&, node<T>*&)
				Postcondition: A new node has been inserted to the stack, and the head now points to this new node.
							   The stack's size has increased by 1.
				First parameter is the data field for the new node.
				Second parameter is set to the new node's link field.

			template<typename T>
			void clear(node<T>*&)
				Precondition: A non-empty stack must exist.
				Postcondition: The current stack contains zero nodes.

*/

#ifndef node_h_
#define node_h_

#include <cstdlib>
#include <iostream>

namespace NSU_Stack
{
	template<typename T>
	class node
	{
	public:
		node(const T& entry, node<T>* link)
		{
			data = entry;
			next = link;
		}
		
		T get_data() const;		

		const node<T>* link() const
		{
			return next;
		}

		node<T>* link()
		{
			return next;
		}

	private:
		T data;
		node<T>* next;
	};

	template<typename T>
	void head_insert(const T&, node<T>*&);

	template<typename T>
	void clear(node<T>*&);
}

#include "node.cpp"
#endif