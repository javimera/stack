/* 
	Programmer: Javier Mera
	Date Modified: 11/10/2013
	File:
		functions.h header file to declare functions to evaluate and translate infix expressions.
		This file includes the stack.h header file in order to work with the stack class.

	// FUNCTIONS

		std::string Infix_to_Postfix(std::string)
			Precondition: A valid infix expression must exist.
						  If the expression has parenthesis, there has to be a balanced number of left and right parenthesis.
			Postcondition: The infix expression has been translated into postfix notation, and is returned from the function.

		double evaluate_expression(std::string)
			Precondition: A valid postfix expression must exist.
			This function evaluates the entire postfix notation and returns the final result as a double number.

*/

#ifndef functions_h_
#define functions_h_

#include "stack.h"

using namespace NSU_Stack;

// Const variables used when parsing an infix expression and evaluating a postfix expression
const char LEFT_PARENTHESIS = '(';
const char RIGHT_PARENTHESIS = ')';
const char DOT = '.';
const char SPACE = ' ';

// Const variables used for each case in a switch statement
const char ADD = '+';
const char SUB = '-';
const char DIV = '/';
const char MUL = '*';

double evaluate_expression(std::string postfix_expression)
{
	// Create a stack to store the numbers from the postfix expression
	stack<double> stack_numbers;

	// Store the result from the entire operation
	double result;

	// Declare a variable of type T to hold the second popped item from the stack
	// The second item popped from the stack will be our first operand in our calculations
	double first_operand;

	// Declare a variable of type T to hold the first popped item from the stack
	// The first item popped from the stack will be our second operand in our calculations
	double second_operand;

	// Loop through the entire postfix expression
	for (int i = 0; i < postfix_expression.length(); i++)
	{
		// Declare a string variable in each loop to hold the next number in the expression
		// A string variable is needed since we may encounter a number with 2 or more digits.
		std::string variable = "";

		// Declare a variable to hold the conversion of the number from a string
		double number;

		// If we encounter a digit in the postfix expression, then loop until we find a nonnumeric character.
		while (isalnum(postfix_expression[i]) || postfix_expression[i] == DOT)
		{
			// Store each numeric character into the string variable
			variable += postfix_expression[i];

			// Increment the postfix index
			i++;

			// Check if the whole number has been stored
			if (!isdigit(postfix_expression[i]))
			{
				// Create an istringstream to put the string into a stream to be able to convert it.
				std::istringstream stream(variable);

				// Store the string number into number variable of type T
				stream >> number;

				// push the converted number into the stack.
				stack_numbers.push(number);
			}
		}

		// If we dont encounter a digit, check if a symbol operator has been reached.
		if (strchr("/*-+", postfix_expression[i]))
		{
			// Store temporarily the symbol operator into a character variable
			char operation = postfix_expression[i];

			// Pop the top number from the stack and store it as a second operand
			second_operand = stack_numbers.pop();

			// Pop the next top number from the stack and store it as a first operand
			first_operand = stack_numbers.pop();

			// Call method to perform a switch statement and evaluate the operator
			stack_numbers.perform_operation(operation, first_operand, second_operand);

			// push the first operand as it is the result of the operation previously performed.
			stack_numbers.push(first_operand);
		}
	}

	// Return the top number from the stack which shuold be the final result.
	result = static_cast<double>(stack_numbers.top());

	return result;
}

std::string Infix_to_Postfix(std::string expression)
{
	// Define a second stack to hold the symbol operators
	stack<char> operators;

	// Define a string variable to where the postfix notation will be saved
	std::string postfix = "";

	// Loop through the entire infix expression
	for (int i = 0; i < expression.length(); i++)
	{
		// Check if the next character is a symbol operator
		if (strchr("+-/*", expression[i]))
		{
			// If its a symbol operator then print the top symbol operator from the stack, then pop it out.
			// Only enter the loop if the top operator in the stack is not a left parenthesis
			while (operators.top() != LEFT_PARENTHESIS)
			{
				// If the top operator is not a left parenthesis, still check if the stack is empty and the order of precedence.
				// If it's empty, then break out of the loop.
				// If the top operator in the stack is of higher precedance than the next character in the expression, then break out of the loop.
				if (operators.is_empty() || (strchr("+-", operators.top()) && (strchr("*/", expression[i]))))
					break;

				// In each loop, pop the top item and print it to the postfix string
				postfix += operators.pop();

				// After printing each symbol, print a space to follow the postfix notation
				postfix += SPACE;
			}

			// Push the next character in the expression into the operator stack.
			operators.push(expression[i]);
		}
		// Check if the next character is a numeric or letter variable or a dot for decimal numbers
		else if (isalnum(expression[i]) || expression[i] == DOT)
		{
			// loop until the next character is not numeric or a letter variable or a dot for decimal numbers
			// The loop will allow to store numbers of 2 or more digits
			while (isalnum(expression[i]) || expression[i] == DOT)
			{
				postfix += expression[i];
				i++;
			}

			// After hitting a non numeric value in the expression, decrement the counter to evaluate this character
			i--;

			// After printing a digit or variable, print a space to follow the postfix notation
			postfix += SPACE;
		}
		// Check if the next character is a left parenthesis
		else if (expression[i] == LEFT_PARENTHESIS)
		{
			// If it's a left parenthesis, then push into the operators stack
			// This will allow to read an expression accordingly if digits/operators are in between parenthesis
			operators.push(expression[i]);
		}
		// Check if the next character is a right parenthesis			
		else if (expression[i] == RIGHT_PARENTHESIS)
		{
			// If it's a right parenthesis, then print and pop everything from the stack until we find its' correspondant left parenthesis
			while (operators.top() != LEFT_PARENTHESIS)
			{
				// In each loop, pop the top item and print it to the postfix string
				postfix += operators.pop();

				// After printing each symbol, print a space to follow the postfix notation
				postfix += SPACE;
			}

			// When reaching the left parenthesis from the previous loop, pop it as we are done with that operand in between parenthesis
			operators.pop();
		}

	}

	// After reading the whole expression, check if there are operators in the stack
	while (!operators.is_empty())
	{
		postfix += operators.pop();
	}

	// Return the expression in postfix notation
	return postfix;
}

#endif