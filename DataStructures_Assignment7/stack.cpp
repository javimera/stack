namespace NSU_Stack
{
	template <typename T>
	// stack constructor
	stack<T>::stack()
	{
		// Set the head pointer to point to NULL for initialization
		this->head_ptr = NULL;

		// Set how many nodes to zero for initialization
		this->how_many = 0;
	}

	template <typename T>
	// stack copy constructor
	stack<T>::stack(const stack<T>& source)
	{ 
		// Set the current stack's head to NULL
		this->head_ptr = NULL;

		// Set the current stack's how many to the source's how many nodes
		this->how_many = source.how_many;

		if(!source.is_empty())
		{
			// Create a temporary dynamic array to hold each source's node's data field in the order they were added in the first place
			T* numbers = new T[this->how_many];						

			// Call helper function to fill the array 
			this->copy_data_fields(numbers, source.head_ptr);

			// Insert in reverse order each array's number into the current stack object
			for(int i = this->how_many - 1; i >= 0 ; i--)
			{
				head_insert(numbers[i], this->head_ptr);
			}

			// Delete the dynamically allocated array
			delete numbers;
		}
	}

	template <typename T>
	void stack<T>::copy_data_fields(T*& numbers, node<T>* temp)
	{
		// Create an index variable to store a value in each array's position
		int index = 0;

		// Loop while there are nodes pointed by temp
		while(temp != NULL)
		{
			// Store the top number pointed by temp into the array
			numbers[index] = temp->get_data();

			// Go to the next node
			temp = temp->link();

			// Increment the index
			index++;
		}		
	}

	template <typename T>	
	stack<T> stack<T>::operator=(const stack<T>& source)
	{
		// Call the copy constructor of the stack being created
		this(source);		
	}

	template <typename T>	
	stack<T>::~stack()
	{
		// Call the clear method from our node class to delete all nodes
		clear(this->head_ptr);

		// Set how many nodes to zero when clearing out a stack
		this->how_many = 0;
	}

	template <typename T>
	// stack method to check if the stack is empty
	bool stack<T>::is_empty() const
	{
		// Returns true if the stack contains at least 1 node
		return this->head_ptr == NULL;
	}

	template <typename T>	
	T stack<T>::top() const
	{
		// Check if there is data to return from the stack
		if(!this->is_empty())
		{
			return this->head_ptr->get_data();
		}
	}

	template <typename T>
	// stack push method to insert a node
	void stack<T>::push(const T& entry)
	{
		head_insert(entry, this->head_ptr);

		// Increment how many since we added a new node
		this->how_many++;
	}

	template <typename T>	
	T  stack<T>::pop()
	{
		// Check if there is anything to pop from the stack
		if(!this->is_empty())
		{
			// Create a temp node object to hold the current head
			node<T>* temp = this->head_ptr;

			// Assign the head its' link field
			this->head_ptr = this->head_ptr->link();

			// store the data pointed by temp
			T data = temp->get_data();

			// delete the temp node
			delete temp;

			// Decrement the amount of nodes in the stack after deletion
			this->how_many--;

			// return the data
			return data;
		}
	}		
	
	template <typename T>
	void stack<T>::perform_operation(const char operation, double& operan1, double operand2) const
	{
		// use a switch statement to see which operation should be performed
		switch(operation)
		{
		case ADD:				
			// Use the first operand as an accumulator and add the second operand to it.
			operan1 += operand2;								
			break;
		case SUB:			
			// Use the first operand as an accumulator and subctract the second operand from it.
			operan1 -= operand2;			
			break;
		case DIV:								
			// Use the first operand as an accumulator and divide the second over it.
			operan1 /= operand2;
			break;
		case MUL:			
			// Use the first operand as an accumulator and multiply the second operand to it.
			operan1 *= operand2;					
			break;			
		}		
	}
}